
#pragma once

int GetRandomNumber(int min, int max);
void EnterMatrix();
void OutputMatrix();
void SumEachRowElementWithElementSideDiagonal();
void SignAlternatingLaw();
void OutputFirstNullElement();


#include "l.h"
#include <iostream>
#include <windows.h>
#include <time.h>
#include <conio.h>
#include <cstdlib>
#include <cmath>
#include <iomanip>

using namespace std;


static int Matrix[17][17];
string Menu[6];
int x, y;
int paragraph;

void GoToXY(int xpos, int ypos)
{
	COORD scrn;
	HANDLE hOuput = GetStdHandle(STD_OUTPUT_HANDLE);
	scrn.X = xpos; scrn.Y = ypos;
	SetConsoleCursorPosition(hOuput, scrn);
}

void SetColor(int text, int background)
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background << 4) | text));
}

void MenuToScreen()
{
	system("CLS");
	for (int i = 0; i <= 5; i++)
	{
		GoToXY(::x, ::y + i);
		cout << ::Menu[i];
	}
	SetColor(2, 15);
	GoToXY(::x, ::y + ::paragraph);
	cout << ::Menu[::paragraph];
	SetColor(0, 15);
}



int GetRandomNumber(int min, int max)
{
	static const double fraction = 1.0 / (static_cast<double>(RAND_MAX)+1.0);
	return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

void EnterMatrix()
{
	for (int i = 0; i < 17; i++)
	for (int j = 0; j < 17; j++)
	{
		Matrix[i][j] = GetRandomNumber(-12, 12);
	}
}

void OutputMatrix()
{
	for (int i = 0; i < 17; i++)
	{
		for (int j = 0; j < 17; j++)
		{
			cout << setw(4) << right << Matrix[i][j];
		}
		cout << endl;
	}
}

void SumEachRowElementWithElementSideDiagonal()
{
	int sumDigitsSideDiagonal = 0, ElementsSideDiagonal = 17;
	for (int i = 0; i < 17; i++)
	{
		sumDigitsSideDiagonal = abs((Matrix[i][ElementsSideDiagonal] % 10) + (Matrix[i][ElementsSideDiagonal] / 10));
		ElementsSideDiagonal = ElementsSideDiagonal - 1;
		for (int j = 0; j < 17; j++)
		{
			Matrix[i][j] = Matrix[i][j] + sumDigitsSideDiagonal;
			cout << setw(6) << Matrix[i][j];
		}
		cout << "\n";
	}
}

void SignAlternatingLaw()
{
	bool check = 1;
	int row;
	cout << "entering row  ";
	cin >> row;
	row++;
	for (int i = 1; i < 17; i++)
	{
		if (Matrix[row][i] * Matrix[row][i - 1] >= 0)
		{
			check = 0;
		}
	}
	if (check)
		cout << "Yes\n";
	else
		cout << "No\n";
}

void OutputFirstNullElement()
{
	bool isExit = 0;
	for (int i = 5; i < 17; i++)
	{
		for (int j = 0; j < 17; j++)
		{
			if (Matrix[i][j] == 0)
			{
				cout << "row " << i + 1 << " , position " << j << "\n";
				isExit = 1;
			}
			if (isExit == 1)
			{
				break;
			}
		}
		if (isExit == 1)
		{
			break;
		}
	}
}